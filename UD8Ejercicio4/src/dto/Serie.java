package dto;

public class Serie {

	//Atributos
	public String titulo;
	public int numeroTemporadas;
	public boolean entregado;
	public String genero;
	public String creador;

	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporadas=" + numeroTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}

	//Constructor por defecto
	public Serie() {
		this.titulo="";
		this.numeroTemporadas=3;
		this.entregado=false;
		this.genero="";
		this.creador="";
	}
	
	//Constructor con el titulo y creador. El resto por defecto
	public Serie(String titulo, String creador) {
		this.titulo=titulo;
		this.numeroTemporadas=3;
		this.entregado=false;
		this.genero="";
		this.creador=creador;
	}
	
	//Constructor con todos los atributos, excepto de entregado
	public Serie(String titulo, int numeroTemporadas, String genero, String creador) {
		this.titulo=titulo;
		this.numeroTemporadas=numeroTemporadas;
		this.entregado=false;
		this.genero=genero;
		this.creador=creador;
	}
}
