import dto.Serie;

public class SerieApp {

	public static void main(String[] args) {
		Serie s1 = new Serie();
		Serie s2 = new Serie("SerieEjemplo", "Quentin Tarantino");
		Serie s3 = new Serie("SerieTest", 5, "Terror", "Stephen king");
		
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
	}

}
